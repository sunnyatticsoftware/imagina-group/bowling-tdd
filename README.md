# bowling-tdd

Explora el sistema de conteo de una partida de bolos para una persona en la web [bowling score calculator](https://www.bowlinggenius.com/)

Vocabulario utilizado:
- partida = game
- tirada = roll
- bolo = pin
- partida a cero = gutter game
- se tiran los 10 bolos en dos intentos = spare
- se tiran los 10 bolos en un intento = strike
- grupo de una o dos tiradas = frame

Observa lo siguiente
- ¿Qué ocurre cuando todas las tiradas son igual a 0 (i.e: gutter game)? ¿Cuántas tiradas se hacen?
- ¿Qué ocurre cuando todas las tiradas son igual a 1? ¿Cuántas tiradas se hacen?
- ¿Qué ocurre cuando se tiran los 10 bolos en dos intentos?
- ¿Qué ocurre cuando se tiran los 10 bolos en un solo inteto?
- ¿Qué ocurre cuando se hace una partida perfecta? ¿Cuántas tiradas habría?

Implementa con TDD, paso a paso, los diferentes escenarios.
