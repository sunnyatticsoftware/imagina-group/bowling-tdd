using FluentAssertions;
using Xunit;

namespace Bowling.UnitTests;

public class BowlingGameTests
{
    [Fact]
    public void Should_Create_Bowling_Game()
    {
        // Given & When
        var sut = new BowlingGame();
        
        // Then
        sut.Should().NotBeNull();
    }
    
    [Fact]
    public void Can_Roll_Gutter_Game()
    {
        // Given
        var sut = new BowlingGame();
        
        // When
        RollMany(sut, 0, 20);
        
        // Then
        sut.Score.Should().Be(0);
    }

    [Fact]
    public void Can_Roll_All_Ones()
    {
        // Given
        var sut = new BowlingGame();
        
        // When
        RollMany(sut, 1, 20);
        
        // Then
        sut.Score.Should().Be(20);
    }

    [Fact]
    public void Can_Roll_Spare()
    {
        // Given
        var sut = new BowlingGame();
        
        // When
        sut.Roll(5);
        sut.Roll(5);
        sut.Roll(3);
        RollMany(sut, 0, 17);
        
        // Then
        sut.Score.Should().Be(16);
    }

    [Fact]
    public void Can_Roll_Strike()
    {
        // Given
        var sut = new BowlingGame();
        
        // When
        sut.Roll(10);
        sut.Roll(3);
        sut.Roll(4);
        RollMany(sut, 0, 16);
        
        // Then
        sut.Score.Should().Be(24);
    }

    [Fact]
    public void Can_Roll_Perfect_Game()
    {
        // Given
        var sut = new BowlingGame();
        
        // When
        RollMany(sut, 10, 12);
        
        // Then
        sut.Score.Should().Be(300);
    }

    private void RollMany(BowlingGame bowlingGame, int pins, int rolls)
    {
        for (var i = 0; i < rolls; i++)
        {
            bowlingGame.Roll(pins);
        }
    }
}