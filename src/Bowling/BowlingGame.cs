public class BowlingGame
{
    private readonly int[] _rolls = new int[21]; // if all are strikes, there are 21 rolls
    private int _currentRoll = 0;
    public int Score
    {
        get
        {
            var score = 0;
            var rollIndex = 0;
            for (var frame = 0; frame < 10; frame++)
            {
                var isStrike = _rolls[rollIndex] == 10;
                if (isStrike)
                {
                    score += GetStrikeScore(rollIndex);
                    rollIndex++;
                }
                else
                {
                    var isSpare = _rolls[rollIndex] + _rolls[rollIndex + 1] == 10;
                    if (isSpare)
                    {
                        score += GetSpareScore(rollIndex);
                        rollIndex += 2;
                    }
                    else
                    {
                        score += GetStandardScore(rollIndex);
                        rollIndex += 2;
                    }
                }
            }
            
            return score;
        }
    }

    private int GetStrikeScore(int rollIndex)
    {
        return _rolls[rollIndex] + _rolls[rollIndex + 1] + _rolls[rollIndex + 2];
    }

    private int GetStandardScore(int rollIndex)
    {
        return _rolls[rollIndex] + _rolls[rollIndex + 1];
    }

    private int GetSpareScore(int rollIndex)
    {
        return _rolls[rollIndex] + _rolls[rollIndex + 1] + _rolls[rollIndex + 2];
    }

    public void Roll(int pins)
    {
        _rolls[_currentRoll] = pins;
        _currentRoll++;
    }
}